FROM registry.fedoraproject.org/fedora:31
LABEL maintainer=simon@krenger.ch

RUN yum update -y && yum upgrade -y && yum clean all && \
    yum install -y nginx && yum clean all && \
    rm -rf /usr/share/nginx/html/* && \
    touch /var/run/nginx.pid && \
    chown -R nginx:nginx /var/run/nginx.pid

WORKDIR /usr/share/nginx/html

USER nginx

ADD css css/
ADD index.html .

ADD nginx.conf /etc/nginx/

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
